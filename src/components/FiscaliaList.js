import React,{ useState, useEffect} from 'react'
import axios from 'axios';
import {Link} from 'react-router-dom'
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import Paper from '@material-ui/core/Paper';
import { createMuiTheme,makeStyles ,ThemeProvider } from '@material-ui/core/styles';
import { esES } from '@material-ui/core/locale';
import { Table, TableBody, TableCell, TableRow, TablePagination, Typography} from '@material-ui/core';

const theme = createMuiTheme({
    typography:{
        fontSize: 14
    }
}, esES);


const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        alignItems: 'center',
    },
    wrapper: {
        margin: theme.spacing(1),
        position: 'relative',
    },
    table: {
    minWidth: 256,
  },
  margin: {
    margin: theme.spacing(1),
  },
}));

export default function FiscaliaList() {
    const [fiscalias, setfiscalias] = useState([])
    const classes = useStyles();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);

    useEffect(() => {
      
        getFisc();
    
    }, [])

    const getFisc = async() =>{
        const res = await axios.get('https://fiscaliasbackend.herokuapp.com/api/fiscalias')
        setfiscalias(res)        
    }
    const deletefn = async(id) =>{
        await axios.delete('https://fiscaliasbackend.herokuapp.com/api/fiscalias/' + id)
        getFisc();
    }

    function handleChangePage(event, page) {
        setPage(page);
    }

    function handleChangeRowsPerPage(event) {
            setRowsPerPage(event.target.value);
        }
    return (
        <div>
            <Typography className="text-64"color="textPrimary">FISCALIAS DE GUATEMALA</Typography>
            <TableContainer component={Paper}>
                    <Table className={classes.table} size="small" aria-label="a dense table">
                        <TableHead>
                        <TableRow>
                            <TableCell>Fiscalia</TableCell>
                            <TableCell>Municipio</TableCell>
                            <TableCell> Direccion</TableCell>
                            <TableCell> Teléfono</TableCell>
                            <TableCell> Descripcion</TableCell>
                            <TableCell> Encargado</TableCell>
                            <TableCell> Fecha Insercion</TableCell>
                            <TableCell> </TableCell>
                            <TableCell> </TableCell>
                        </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                        fiscalias.data &&(
                                        fiscalias.data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) =>{
                                            return (
                                                <TableRow 
                                                className="h-12 cursor-pointer"
                                                hover 
                                                role="checkbox" 
                                                tabIndex={-1} 
                                                //key={row.FOLIO_SOL}
                                                //onClick={event => handleClick(row)} 
                                                key={row._id}
                                                >                                                
                                                    <TableCell component="th" scope="row">
                                                        {row.fiscalia}
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">
                                                    {row.municipio}
                                                        
                                                    </TableCell>                                                    
                                                    <TableCell component="th" scope="row">                                                        
                                                    {row.direccion}
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">                                                        
                                                    {row.telefono}
                                                    </TableCell>  
                                                    <TableCell component="th" scope="row">                                                        
                                                    {row.content}
                                                    </TableCell> 
                                                    <TableCell component="th" scope="row">                                                        
                                                    {row.author}
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">                                                        
                                                    {new Date(row.date).toLocaleDateString("es-ES")}
                                                    </TableCell>        
                                                       
                                                    <TableCell component="th" scope="row">                                                        
                                                    {new Date(row.date).toLocaleDateString("es-ES")}
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">                                                        
                                                    <button 
                                                    className="btn btn-danger btn-sm"
                                                    onClick={() => deletefn(row._id)}
                                                    >Delete</button>
                                                    </TableCell>    
                                                    <TableCell component="th" scope="row">                                                        
                                                    <Link className="btn btn-warning btn-sm" to={"/edit/" + row._id}>
                                                        Editar
                                                        </Link>
                                                    </TableCell>                                                                                                            
                                                </TableRow> 
                                            )
                                        })
                                        )
                                    }                            
                            

                        </TableBody>
                    </Table>
                    <ThemeProvider theme={theme}>
            <TablePagination
                                component="div"
                                
                                count={fiscalias.data?fiscalias.data.length: 0}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                backIconButtonProps={{
                                    'aria-label': 'Previous Page'
                                }}
                                nextIconButtonProps={{
                                    'aria-label': 'Next Page'
                                }}
                                onChangePage={handleChangePage}
                                onChangeRowsPerPage={handleChangeRowsPerPage}
                            />
            </ThemeProvider>
                </TableContainer>
        {/* <div className="col-md-12 p-2">
            <div className="card-header">
                <h3>Fiscalias de Guatemala</h3>
            </div>
            

            <div className="card-body">
            <table className="table table-striped  table-sm table-responsive">
                 <thead className="table-dark">
                    <tr>                            
                    <th scope="col">Nombre</th>
                    <th scope="col">Municipio</th>
                    <th scope="col">Direccion</th>
                    <th scope="col">Telefono</th>
                    <th scope="col">Descripcion</th>
                    <th scope="col">Encargado</th>
                    <th scope="col">Fecha Insercion</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    {fiscalias.map( fisc =>
                    <tr key={fisc._id}>
                    <td>{fisc.fiscalia}</td>
                     <td>{fisc.municipio}</td>
                     <td>{fisc.direccion}</td>
                     <td>{fisc.telefono}</td>
                     <td>{fisc.content}</td>
                     <td>{fisc.author}</td>
                     <td>{new Date(fisc.date).toLocaleDateString("es-ES")}</td>
                     <td><button 
                     className="btn btn-danger btn-sm"
                     onClick={() => deletefn(fisc._id)}
                     >Delete</button></td>
                     <td>
                         <Link className="btn btn-warning btn-sm" to={"/edit/" + fisc._id}>
                         Editar
                         </Link>
                         </td>
                     </tr>
                     
                    )}
                    
                </tbody>
            </table>
        </div>
    </div> */}
    </div>
    )
}

