import React, { useState, useEffect } from 'react'
import axios from 'axios'
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import {useHistory} from 'react-router-dom'

export default function CreateFiscalias(props) {
    let history = useHistory()
    const [users, setusers] = useState([])
    const [userSelected, setuserSelected] = useState('')
    const [estado, setestado] = useState({
                                          fiscalia: '',
                                          municipio: '',
                                          direccion: '',
                                          telefono: '',
                                          content: '',
                                          date: new Date(),
                                          editing: false,
                                          _id: ''  
                                            });

                                    
    
           
    useEffect(() => {    
        
        if(props.match.params.id){
            async function traer(){
            const res = await axios.get('https://fiscaliasbackend.herokuapp.com/api/fiscalias/' + props.match.params.id);
            setuserSelected(res.data.author)
            setestado({

                userSelected: res.data.author,
                fiscalia: res.data.fiscalia,
                municipio: res.data.municipio,
                direccion: res.data.direccion,
                telefono: res.data.telefono,
                content: res.data.content,
                date: new Date(res.data.date),
                editing: true,
                _id: props.match.params.id

            })
        }
        traer()
        }
    }, [ props.match.params.id])
    

    useEffect(() => {
        
        axios.get('https://fiscaliasbackend.herokuapp.com/api/users/')
        .then(res =>{
            setusers(res.data.map(user => user.username))
            setuserSelected(res.data[0].username)
            
        })
        
         
}, [])
        const onSubmit = async(e) =>{
                        e.preventDefault();
                        const newFiscalia ={
                            fiscalia: estado.fiscalia,
                            municipio: estado.municipio,
                            direccion: estado.direccion,
                            telefono: estado.telefono,
                            date: estado.date,
                            content: estado.content,
                            author: userSelected
                        };
                        if(estado.editing){
                               await axios.put('https://fiscaliasbackend.herokuapp.com/api/fiscalias/' + estado._id, newFiscalia);
                        }else{
                            await axios.post('https://fiscaliasbackend.herokuapp.com/api/fiscalias/', newFiscalia);
                        }
                        
                         
                        history.push('/fiscalist')
                    }

        const onInputOnChange = e =>{
                        setestado({
                            ...estado,
                            [e.target.name]: e.target.value
                        })}
        const onChangeuser = e =>{
        
            setuserSelected(e.target.value)
        }
                
        const onChangeDate = date =>{
                        setestado({
                            ...estado,
                            date
                        })
                    }
                
    return (
<div className="col-md-6 offset-md-3">
                <div className="card card-body">
                    <h4>Crear Fiscalía</h4>
                    <div className="form-group">
                        <select
                            className="form-control"
                            name="userSelected"
                            value={userSelected}
                            onChange={onChangeuser}   
                        >
                            {
                                users.map( user => 
                                <option key={user}>
                                    {user}
                                </option>)
                            }
                        </select>
                    </div>

                    <div className="form-group">
                        <input 
                        type="text" 
                        className="form-control"
                        placeholder="Fiscalia"
                        name="fiscalia"
                        required
                        value={estado.fiscalia}
                        onChange={onInputOnChange}
                        />
                    </div>
                    <div className="form-group">
                        <input 
                        type="text" 
                        className="form-control"
                        placeholder="Municipio"
                        name="municipio"
                        value={estado.municipio}
                        required
                        onChange={onInputOnChange}
                        />
                    </div>
                    <div className="form-group">
                        <input 
                        type="text" 
                        className="form-control"
                        placeholder="Dirección"
                        name="direccion"
                        value={estado.direccion}
                        required
                        onChange={onInputOnChange}
                        />
                    </div>
                    <div className="form-group">
                        <input 
                        type="text" 
                        className="form-control"
                        placeholder="Telefono"
                        name="telefono"
                        required
                        value={estado.telefono}
                        onChange={onInputOnChange}
                        />
                    </div>
                    <div className="form-group">
                        <textarea name="content" 
                         className="form-control"
                         placeholder="Descripcion"
                         required
                         value={estado.content}
                         onChange={onInputOnChange}
                        >                            
                        </textarea>
                    </div>
                    <div className="form-group">
                        <DatePicker 
                            className="form-control"
                            selected={estado.date}
                            onChange={onChangeDate}
                        />
                    </div>
                    <form onSubmit={onSubmit}>
                        <button type="submit" className="btn btn-primary">
                            Guardar    
                        </button>
                    </form>
                </div>
            </div>
    )
}


