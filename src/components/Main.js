
import React from 'react'

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    padding: "10px",
    '& > *': {
      margin: theme.spacing(1),
      width: theme.spacing(128),
      height: theme.spacing(90),
    },
  },
}));




export default function Main() {
    const classes = useStyles();
    return (
        <div>
                <div className="bd-example">
                    <div id="carouselExampleCaptions" className="carousel slide" data-ride="carousel">
                        <ol className="carousel-indicators">
                            <li data-target="#carouselExampleCaptions" data-slide-to="0" className="active"></li>
                            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                            <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                        </ol>
                        <div className="carousel-inner">
                            <div className="carousel-item active">
                            <div className={classes.root}>
      
                                    <Paper elevation={3} >
                                    <div className="m-8">
                                    <Typography  variant="h4" gutterBottom>
                                        Prueba Tecnica de Fiscalia
                                    </Typography>
                                    <Typography variant="h6" color="primary" gutterBottom>
                                        Desarrollador Mauro Jutzutz Sepet
                                    </Typography>
                                    <Typography variant="body1" gutterBottom>
                                        mauro.jutzutz@gmail.com
                                    </Typography>
                                    <Typography variant="body1" gutterBottom>
                                        Tel. 57599466
                                    </Typography>
                                    <Typography variant="body1" gutterBottom>
                                    linkedin.com/in/maurojuztutz
                                    </Typography>
                                    <Typography variant="body1" gutterBottom>
                                    github.com/maurojutzutzsepet
                                    </Typography>
                                    
                                    </div>
                                    <div className="m-8">
                                    <Typography  variant="h5" gutterBottom>
                                        Documentación de Backend 
                                    </Typography>
                                    <Typography variant="h6" color="primary" gutterBottom>
                                    <a href="https://fiscaliasbackend.herokuapp.com/api-docs/">
                                    https://fiscaliasbackend.herokuapp.com/api-docs/
                                    </a>
                                    </Typography>
                                    </div>
                                    <div className="m-8">
                                    <Typography  variant="h5" gutterBottom>
                                        Requerimientos 
                                    </Typography>
                                    <Typography variant="body1" gutterBottom>
                                        Se desea crear una plataforma, para la gestion y control de la información de las fiscalias con las que cuenta el Ministerio Publico, 
                                        estas se encuentran en todo el pais, por lo que las acciones dentro de la aplicación buscan resumir.
                                        Como objetivo principal se necesita la información basica de las fiscalia, por ejemplo: nombre de la fiscalía, dirección, teléfono, descripción.
                                        de la fiscalia entre otras, dentro de la plataforma se requiere la administracion de los fiscales, por lo que se plantea el modulo de agregar Fiscal,
                                        esta puede crear fiscalias, junto con acciones de editar y eliminar las fiascalias que deseé.

                                    </Typography>
                                    </div>
                                    <div className="m-8">
                                    <Typography  variant="h5" gutterBottom>
                                        Desarrollo de la plataforma
                                    </Typography>
                                    <Typography variant="body1" gutterBottom>
                                        Dentro de los requerimientos, se desarrolló la plataforma, totalmente con sercivios REST, a nivel de backend y frontend, en la siguiente presentación se mostraran las herramientas que se utilizaron..
                                    </Typography>
                                    </div>
                                    </Paper>
                                    </div>
                                <div className="carousel-caption d-none d-md-block">
                                   
                                </div>
                            </div>
                            <div className="carousel-item">
                            <div className={classes.root}>
                            
                            <Paper elevation={3} >
                            <div className="m-8">
                            <Typography variant="h4" gutterBottom>
                                Herramientas de desarrollo
                            </Typography>
                            <Typography  variant="h6" color="primary" gutterBottom>
                                Esta plataforma esta gestionada con Integración Continua, con el ciclo de desarrollo de Devoops gracias a GitlabPages
                            </Typography>
                            <Typography  variant="h6"  gutterBottom>
                                Base de datos 
                            </Typography>
                            <Typography  variant="body1" gutterBottom>
                                Mongo DB
                            </Typography>
                            <Typography  variant="h6"  gutterBottom>
                                Frontend 
                            </Typography>
                            <Typography  variant="body1" gutterBottom>
                                React Hooks {"&&"} React Redux
                            </Typography>
                            
                            <Typography  variant="h6"  gutterBottom>
                                Servidor Cloud
                            </Typography>
                            <Typography  variant="body1" gutterBottom>
                                Heroku
                            </Typography>
                            <Typography  variant="h6"  gutterBottom>
                                Entorno de desarrollo de backend
                            </Typography>
                            <Typography  variant="body1" gutterBottom>
                                Node js
                            </Typography>
                            <Typography  variant="h6" gutterBottom>
                                Documentación de Backend 
                            </Typography>
                            <Typography  variant="body1" gutterBottom>
                                Swagger
                            </Typography>
                            <Typography  variant="h6" color="primary" gutterBottom>
                                Integracion continua
                            </Typography>
                            <Typography  variant="body1" gutterBottom>
                                Gitlab pages ofrece uno de sus servicios gratuitos para publicar sitios estaticos, ademas de este servicio, 
                                nos ofrece almacenar o alojar nuestro sitio en un contenedor de docker, de la misma manera podemos gestionar todo el 
                                ciclo de desarrollo de intergracion continua
                                
                                
                            </Typography>
                            </div>
                            </Paper>
                            </div>
                                <div className="carousel-caption d-none d-md-block">
                                    
                                </div>
                            </div>
                            <div className="carousel-item">
                                <img src="https://res.cloudinary.com/cao/image/upload/v1593616361/JES/bena_ycvczf.png" className="d-block w-100" alt="..." />
                                <div className="carousel-caption d-none d-md-block">
                                    
                                </div>
                            </div>
                        </div>
                        <a className="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span className="sr-only">Previous</span>
                        </a>
                        <a className="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                            <span className="carousel-control-next-icon" aria-hidden="true"></span>
                            <span className="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
    )
}

