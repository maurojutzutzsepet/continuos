import React, { useState, useEffect } from 'react'
import axios from 'axios';

export default function CreateUser() {
    const [users, setusers] = useState([])
    const [username, setusername] = useState('')
    useEffect(() => {
        getdocs()
    }, [])

   const getdocs = async() =>{
        const res = await axios.get('https://fiscaliasbackend.herokuapp.com/api/users/');
        setusers(res.data)
   }

   const onChangeUserName = (e) =>{
    setusername(e.target.value)
    }
    
    const onSubmit = async e => {
        e.preventDefault();
        await axios.post('https://fiscaliasbackend.herokuapp.com/api/users', {
            username: username
        })
        setusername('')
        getdocs();        
    }

   const deleteUser = async(id) =>{
        await axios.delete('https://fiscaliasbackend.herokuapp.com/api/users/' + id)
        getdocs();
    }

    return (
        <div className="row">
                <div className="col-md-4">
                    <div className="card card-body">
                       <h3>Crear Nuevo Usuario</h3> 
                        <form onSubmit={onSubmit}>
                            <div className="form-group">
                                <input 
                                type="text" 
                                className="form-control" 
                                value={username}
                                onChange={onChangeUserName}/>
                            </div>
                            <button type="submit" className="btn btn-primary">
                                Guardar 
                            </button>
                        </form>
                    </div>
                </div> 
                <div className="col-md-8">
                    <ul className="list-group">
                        
                        {
                            users.map(user =>(
                                <li 
                                className="list-group-item list-group-item-action" 
                                key={user._id}
                                onDoubleClick={() => deleteUser(user._id)}
                                >
                                    {user.username}                                    
                                </li>
                            ))
                        }
                    </ul>
                </div>
            </div>

    )
}




